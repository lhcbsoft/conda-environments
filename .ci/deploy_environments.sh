#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

TASK_UUID=$(curl --silent -L -X PUT  -H "Authorization: Bearer ${REQUEST_DEPLOY_INSTALL_TOKEN}" "https://lhcb-core-tasks.web.cern.ch/hooks/conda-deploy/deploy/${CI_PIPELINE_ID}/" | cut -d '"' -f 2)
echo "Sucessfully sent task with ID ${TASK_UUID}"
while true; do
    CURRENT_STATUS=$(curl --silent -L "https://lhcb-core-tasks.web.cern.ch/tasks/status/$TASK_UUID" | cut -d '"' -f 2);
    echo "$(date -u): Status is ${CURRENT_STATUS}";
    if [[ "${CURRENT_STATUS}" == "SUCCESS" ]]; then
        exit 0;
    elif [[ "${CURRENT_STATUS}" == "FAILURE" ]]; then
        exit 1;
    else
        sleep 30;
    fi
done
