#!/usr/bin/env bash
source /cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/prod/bin/activate
set -euo pipefail
IFS=$'\n\t'

for ENV_NAME in $(.ci/find_new_environments.py); do
    PREFIX=/cvmfs/lhcbdev.cern.ch/conda/envs/${ENV_NAME}/$(date -u '+%Y-%m-%d_%H-%M')/linux-64
    echo "Creating environment for ${ENV_NAME} in ${PREFIX}"
    conda env create --prefix "${PREFIX}/" --file "environments/${ENV_NAME}.yaml"
    touch "${PREFIX}/.cvmfscatalog"
    conda env export --prefix "${PREFIX}/" | tee "${PREFIX}.yaml"
done

# Create the artifact tarballs and upload them
python .ci/create-artifacts.py
du -h build-artifacts/*

if [ -n "${REQUEST_DEPLOY_URL_TOKEN+x}" ]; then
    python .ci/upload-artifacts.py;
fi
