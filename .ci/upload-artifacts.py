#!/usr/bin/env python
import os

import requests
from requests_toolbelt import MultipartEncoder

pipeline_id = os.environ["CI_PIPELINE_ID"]
response = requests.get(
    f"https://lhcb-core-tasks.web.cern.ch/hooks/conda-deploy/artifact_url/{pipeline_id}/",
    headers={"Authorization": f"Bearer {os.environ['REQUEST_DEPLOY_URL_TOKEN']}"}
)
response.raise_for_status()
data = response.json()

with open(f"build-artifacts/{pipeline_id}.tar.ztd", "rb") as fp:
    files = {"file": (f"{pipeline_id}.tar.ztd", fp, 'application/octet-stream')}
    m = MultipartEncoder(fields=dict(**data["fields"], **files))
    response = requests.post(data["url"], data=m, headers={'content-type': m.content_type})
response.raise_for_status()
