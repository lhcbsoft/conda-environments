#!/usr/bin/env bash

# Install CVMFS
yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum install -y cvmfs
# TODO: Stop using the test instance of lhcbdev
echo "CVMFS_SERVER_URL=http://cvmfs-lhcbdev.s3.cern.ch/cvmfs/lhcbdev.cern.ch" > /etc/cvmfs/config.d/lhcbdev.cern.ch.conf
echo "CVMFS_HTTP_PROXY=DIRECT" >> /etc/cvmfs/default.conf
mkdir /real_cvmfs
mount -t cvmfs lhcbdev.cern.ch /real_cvmfs
# Prepare mocked CVMFS
mkdir -p "/cvmfs/lhcbdev.cern.ch/conda/envs"
mkdir -p "/cvmfs/lhcbdev.cern.ch/conda/pkgs/linux-64"
mkdir -p "/cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/"
# Symlink the main miniconda installation
ln -s "/real_cvmfs/conda/miniconda/linux-64/prod" "/cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/"
ln -s "/real_cvmfs/conda/miniconda/linux-64/$(basename "$(readlink /real_cvmfs/conda/miniconda/linux-64/prod)")" "/cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/"
ln -s "/real_cvmfs/conda/.condarc" "/cvmfs/lhcbdev.cern.ch/conda/"
# Activate miniconda
source /cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/prod/bin/activate
# Add the existing CVMFS packages directory to the search list
conda config --add pkgs_dirs /real_cvmfs/conda/pkgs/linux-64/
