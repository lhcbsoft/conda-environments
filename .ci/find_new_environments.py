#!/usr/bin/env python3
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__all__ = [
    'changes',
]

import os
from os.path import abspath, dirname, join, relpath, splitext
import re
import logging as log

import git

# log.getLogger().setLevel(log.DEBUG)


def changes(repo_base, prefix='.', depth=0, regex=None, is_merge_request=False):
    repo_base = abspath(repo_base)
    assert not prefix.startswith('/'), prefix
    prefix = relpath(abspath(join(repo_base, prefix)), repo_base)
    log.debug('Running git.changes(repo_base=%s, prefix=%s,)', repo_base, prefix)

    repo = git.Repo(repo_base)
    if regex:
        regex = re.compile('^%s$', regex)

    if is_merge_request:
        common_ancestor = repo.merge_base('HEAD', 'origin/master')
        assert len(common_ancestor) == 1, common_ancestor
        common_ancestor = common_ancestor[0]
        diffs = common_ancestor.diff('HEAD')
    else:
        diffs = repo.commit('HEAD~1').diff('HEAD')

    changes = set()
    for diff in diffs:
        log.debug('Found change: a_path=%s b_path=%s new_file=%s '
                  'renamed_file=%s deleted_file=%s', diff.a_path, diff.b_path,
                  diff.new_file, diff.renamed_file, diff.deleted_file)

        if diff.deleted_file:
            continue
        full_path = diff.b_path

        if full_path is None:
            continue
        if not (prefix == '.' or full_path.startswith(prefix+os.sep)):
            continue
        full_path = relpath(full_path, prefix)
        result = full_path
        if depth:
            result = join(*full_path.split(os.sep)[:depth])
        if regex:
            match = regex.match(full_path)
            log.debug('Pattern "%s" matches %s with %s', regex.pattern, full_path, match)
            if not match:
                continue
        changes.add(result)

    return sorted(changes)


def main():
    is_merge_request = False
    repo_base = dirname(dirname(__file__))
    for fn in changes(repo_base, is_merge_request=is_merge_request):
        log.debug("Found modified file %s", fn)
        fn = relpath(fn, join(repo_base, 'environments'))
        if fn.startswith('../'):
            continue
        env_name, ext = splitext(fn)
        if ext == ".yaml":
            print(env_name)


if __name__ == '__main__':
    main()
